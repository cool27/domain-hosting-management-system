<?php

use App\Http\Controllers\Auth\LoginController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Backend\IndexController;
use App\Http\Controllers\Backend\ClassController;
use App\Http\Controllers\Backend\RoleController;
use App\Http\Controllers\Backend\SectionsController;
use App\Http\Controllers\Backend\UserController;
use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\Backend\AdminsController;
use App\Http\Controllers\Backend\ClientController;
use App\Http\Controllers\Backend\DomainController;
use App\Http\Controllers\Backend\HostingCategoryController;
use App\Http\Controllers\Backend\HostingController;
use App\Http\Controllers\Backend\SystemSettingsController;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Auth::routes();
// login routes
Route::get('admin/login', [LoginController::class, 'showLoginForm'])->name('admin.login');
Route::post('admin/login/submit', [LoginController::class, 'login'])->name('admin.login.submit');


// admin routes
Route::middleware(['auth:admin'])->group(function () {
    Route::group(['prefix' => 'admin'], function () {
        Route::get('/', [IndexController::class, 'index'])->name('dashboard');
        Route::resource('roles', RoleController::class);
        Route::resource('users', UserController::class);
        Route::resource('admin', AdminsController::class);
        Route::resource('class', ClassController::class);
        Route::resource('client', ClientController::class);
        Route::resource('domain', DomainController::class);
        Route::resource('section', SectionsController::class);
        Route::resource('hosting', HostingController::class);
        Route::post('/hosting/{id}', [HostingController::class, 'destroy'])->name('myhosting.destroy');
        Route::resource('hostingcategory', HostingCategoryController::class);
        Route::resource('systemsetting', SystemSettingsController::class);

        Route::get('/view-invoice/{id}', [HostingController::class, 'ViewInvoice'])->name('view.invoice');

        // logout routes
        Route::post('/logout/submit', [LoginController::class, 'logout'])->name('admin.logout.submit');
        Route::post('/payment', [HostingController::class, 'MakePayment'])->name('make-payment');
        Route::post('/payment/{id}', [HostingController::class, 'UpdatePayment'])->name('update-payment');
        Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

        //Forget Password Routes
        Route::get('/password/reset', [ForgotPasswordController::class, 'showLinkRequestForm'])->name('admin.password.reset');
        Route::post('/passwordreset/submit', [LoginController::class, 'reset'])->name('admin.password.update');
    });
});
