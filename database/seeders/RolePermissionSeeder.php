<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolePermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //create roles

        //create roles if they don't already exist
        $roleSuperAdmin = Role::firstOrCreate(['name' => 'superadmin', 'guard_name' => 'admin']);
        $roleAdmin = Role::firstOrCreate(['name' => 'admin', 'guard_name' => 'admin']);
        $roleEditor = Role::firstOrCreate(['name' => 'editor', 'guard_name' => 'admin']);
        $roleUser = Role::firstOrCreate(['name' => 'user', 'guard_name' => 'admin']);

        //persmissoin list as array
        $permissions = [

            [
                'group_name' => 'dashboard',
                'permissions' => [
                    'dashboard.view',
                    'dashboard.edit',
                ]
            ],
            [
                'group_name' => 'client',
                'permissions' => [
                    'client.create',
                    'client.view',
                    'client.edit',
                    'client.delete',
                ]
            ],
            [
                'group_name' => 'admin',
                'permissions' => [
                    'admin.create',
                    'admin.view',
                    'admin.edit',
                    'admin.delete',
                ]
            ],

            [
                'group_name' => 'role',
                'permissions' => [
                    //Role permission
                    'role.create',
                    'role.view',
                    'role.edit',
                    'role.delete',
                ]
            ],
            [
                'group_name' => 'domain',
                'permissions' => [
                    //Domain permission
                    'domain.create',
                    'domain.view',
                    'domain.edit',
                    'domain.delete',
                ]
            ],
            [
                'group_name' => 'hostingcategory',
                'permissions' => [
                    //Hosting Category permission
                    'hostingcategory.create',
                    'hostingcategory.view',
                    'hostingcategory.edit',
                    'hostingcategory.delete',
                ]
            ],
            [
                'group_name' => 'hosting',
                'permissions' => [
                    //Hosting permission
                    'hosting.create',
                    'hosting.view',
                    'hosting.edit',
                    'hosting.delete',
                ]
            ],
            [
                'group_name' => 'setting',
                'permissions' => [
                    //Hosting permission
                    'setting.create',
                    'setting.view',
                    'setting.edit',
                    'setting.delete',
                ]
            ],


        ];

        //Create and Assign Permissions
        for ($i = 0; $i < count($permissions); $i++) {
            $permissionGroup = $permissions[$i]['group_name'];
            for ($j = 0; $j < count($permissions[$i]['permissions']); $j++) {
                //create permisson


                $permission = Permission::firstOrCreate(['name' => $permissions[$i]['permissions'][$j], 'guard_name' => 'admin', 'group_name' => $permissionGroup]);
                $roleSuperAdmin->givePermissionTo($permission);
                $permission->assignRole($roleSuperAdmin);
            }
        }
    }
}
