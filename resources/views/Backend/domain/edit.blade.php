@extends('Backend.dashboard.main')
@section('title', 'Domain Edit')
@section('content')
    <div class="content container-fluid">

        <div class="page-header">
            <div class="row align-items-center">
                <div class="col">
                    <h3 class="page-title">Edit domain</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('domain.index') }}">domains</a></li>
                        <li class="breadcrumb-item active">Edit domain</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">

                        <form method="POST" action="{{ route('domain.update', $domain->id) }}">
                            @csrf
                            @method('put')
                            <div class="row">
                                <div class="col-12 col-sm-6 col-md-3">
                                    <div class="form-group">

                                        <select class="form-control select" name="client_id" id="exampleFormControlSelect1">
                                            <option>-- choose client -- </option>
                                            @if ($client)
                                                @foreach ($client as $key => $value)
                                                    <option value="{{ $value }}"
                                                        {{ $value == $domain->client_id ? 'selected' : '' }}>
                                                        {{ $key }}</option>
                                                @endforeach
                                            @endif


                                        </select>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-3">
                                    <div class="form-group local-forms">
                                        <label>Domain Name <span class="login-danger">*</span></label>
                                        <input type="text" class="form-control @error('name')is-invalid @enderror"
                                            name="name" value="{{ $domain->name }}" placeholder="Domain Name">
                                        @error('name')
                                            <p style="color: indianred;margin-top: 5px">{{ $message }}</p>
                                        @enderror
                                    </div>

                                </div>
                                <div class="col-12 col-sm-6 col-md-3">
                                    <div class="form-group local-forms">
                                        <label>Domain Issuer <span class="login-danger">*</span></label>
                                        <input type="text" class="form-control @error('issuer')is-invalid @enderror"
                                            name="issuer" value="{{ $domain->issuer }}" placeholder="Domain issuer">
                                        @error('issuer')
                                            <p style="color: indianred;margin-top: 5px">{{ $message }}</p>
                                        @enderror
                                    </div>

                                </div>

                                <div class="col-12 col-sm-6 col-md-3">
                                    <div class="form-group local-forms">
                                        <label>Domain Issue Date <span class="login-danger">*</span></label>
                                        <input class="form-control datetimepicker" type="date" placeholder="DD-MM-YYYY"
                                            name="issue_date" value="{{ $domain->issue_date }}">
                                        @error('issue_date')
                                            <p style="color: indianred;margin-top: 5px">{{ $message }}</p>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-3">
                                    <div class="form-group local-forms">
                                        <label>Domain Expiry Date<span class="login-danger">*</span></label>
                                        <input type="date" class="form-control @error('expiry_date')is-invalid @enderror"
                                            name="expiry_date" value="{{ $domain->expiry_date }}"
                                            placeholder="Domain expiry date">
                                        @error('expiry_date')
                                            <p style="color: indianred;margin-top: 5px">{{ $message }}</p>
                                        @enderror
                                    </div>

                                </div>

                                <div class="col-12 col-sm-4">
                                    <div class="form-group" data-select2-id="11">

                                        <select class="select select2-hidden-accessible" name="status" data-select2-id="1"
                                            tabindex="-1" aria-hidden="true">

                                            <option value="active" {{ $domain->status == 'active' ? 'selected' : '' }}
                                                data-select2-id="17">Active</option>
                                            <option value="inactive" {{ $domain->status == 'inactive' ? 'selected' : '' }}
                                                data-select2-id="18">Inactive</option>


                                        </select>
                                        @error('gender')
                                            <p style="color: indianred;margin-top: 5px">{{ $message }}</p>
                                        @enderror
                                    </div>

                                </div>
                                <div class="col-12">
                                    <div class="student-submit">
                                        <button type="submit" class="btn btn-primary">Save Domain</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
