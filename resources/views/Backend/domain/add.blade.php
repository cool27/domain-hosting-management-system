@extends('Backend.dashboard.main')
@section('title', 'Domain Add')
@section('content')
    <div class="content container-fluid">

        <div class="page-header">
            <div class="row align-items-center">
                <div class="col">
                    <h3 class="page-title">Add domain</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('domain.index') }}">domains</a></li>
                        <li class="breadcrumb-item active">Add domain</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">

                        <form method="POST" action="{{ route('domain.store') }}">
                            @csrf
                            <div class="row">
                                <div class="col-12 col-sm-6 col-md-4">
                                    <div class="form-group" data-select2-id="11">

                                        <select class="select select2-hidden-accessible" name="client_id"
                                            data-select2-id="1" tabindex="-1" aria-hidden="true">
                                            <option value="" selected disabled>-- choose client -- </option>
                                            @if ($client->isNotEmpty())
                                                @foreach ($client as $key => $value)
                                                    <option value="{{ $value }}"
                                                        {{ old('client_id') == $value ? 'selected' : '' }}>
                                                        {{ $key }}
                                                    </option>
                                                @endforeach
                                            @endif


                                        </select>
                                        @error('client_id')
                                            <p style="color: indianred;margin-top: 5px">{{ $message }}</p>
                                        @enderror
                                    </div>

                                </div>
                                <div class="col-12 col-sm-6 col-md-4">
                                    <div class="form-group local-forms">
                                        <label>Domain Name <span class="login-danger">*</span></label>
                                        <input type="text" class="form-control @error('name')is-invalid @enderror"
                                            name="name" value="{{ old('name') }}" placeholder="Domain Name">
                                        @error('name')
                                            <p style="color: indianred;margin-top: 5px">{{ $message }}</p>
                                        @enderror
                                    </div>

                                </div>
                                <div class="col-12 col-sm-6 col-md-4">
                                    <div class="form-group local-forms">
                                        <label>Domain Issuer <span class="login-danger">*</span></label>
                                        <input type="text" class="form-control @error('issuer')is-invalid @enderror"
                                            name="issuer" value="{{ old('issuer') }}" placeholder="Domain issuer">
                                        @error('issuer')
                                            <p style="color: indianred;margin-top: 5px">{{ $message }}</p>
                                        @enderror
                                    </div>

                                </div>
                                <div class="settings-form mt-0">

                                    <div class="form-group">
                                        <p class="pay-cont" style="font-weight:700">Is this an free domain?</p>
                                        <label class="custom_radio me-4">
                                            <input type="radio" name="free" id="" value="no"
                                                checked="">
                                            <span class="checkmark"></span> No
                                        </label>
                                        <label class="custom_radio">
                                            <input type="radio" name="free" id="" value="yes">
                                            <span class="checkmark"></span> Yes
                                        </label>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-12 col-sm-6 col-md-6" id="cost_price">
                                        <div class="form-group local-forms">
                                            <label>Cost Price <span class="login-danger">*</span></label>
                                            <input type="number"
                                                class="form-control @error('cost_price')is-invalid @enderror"
                                                name="cost_price" value="{{ old('cost_price') }}" placeholder="Cost Price">
                                            @error('cost_price')
                                                <p style="color: indianred;margin-top: 5px">{{ $message }}</p>
                                            @enderror
                                        </div>

                                    </div>
                                    <div class="col-12 col-sm-6 col-md-6" id="selling_price">
                                        <div class="form-group local-forms">
                                            <label>Selling Price <span class="login-danger">*</span></label>
                                            <input type="number"
                                                class="form-control @error('selling_price')is-invalid @enderror"
                                                name="selling_price" value="{{ old('selling_price') }}"
                                                placeholder="Selling Price">
                                            @error('selling_price')
                                                <p style="color: indianred;margin-top: 5px">{{ $message }}</p>
                                            @enderror
                                        </div>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 col-sm-6 col-md-6">
                                        <div class="form-group local-forms">
                                            <label>Domain Issue Date <span class="login-danger">*</span></label>
                                            <input class="form-control datetimepicker" type="date"
                                                placeholder="DD-MM-YYYY" name="issue_date" value="{{ old('issue_date') }}">
                                            @error('issue_date')
                                                <p style="color: indianred;margin-top: 5px">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6 col-md-6">
                                        <div class="form-group local-forms">
                                            <label>Domain Expiry Date<span class="login-danger">*</span></label>
                                            <input type="date"
                                                class="form-control @error('expiry_date')is-invalid @enderror"
                                                name="expiry_date" value="{{ old('expiry_date') }}"
                                                placeholder="Domain expiry date">
                                            @error('expiry_date')
                                                <p style="color: indianred;margin-top: 5px">{{ $message }}</p>
                                            @enderror
                                        </div>

                                    </div>
                                </div>

                                <div class="col-12">
                                    <div class="student-submit">
                                        <button type="submit" class="btn btn-primary">Save Domain</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $(document).ready(function() {
            // $('#FreeDomain').hide();
            $("input[type='radio']").click(function() {
                var radioValue = $("input[name='free']:checked").val();
                if (radioValue == 'yes') {
                    $('#selling_price').show();
                    $('#cost_price').hide();
                    $("input[name='cost_price']").val('')
                } else if (radioValue == 'no') {
                    $('#selling_price').show();
                    $('#cost_price').show();
                    //$('#mySelectField').val($('#mySelectField option:first').val());
                }
            });
        });
    </script>
@endsection
