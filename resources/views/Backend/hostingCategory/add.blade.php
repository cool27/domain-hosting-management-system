@extends('Backend.dashboard.main')
@section('title', 'Hosting Package Add')
@section('content')
    <div class="content container-fluid">

        <div class="page-header">
            <div class="row align-items-center">
                <div class="col">
                    <h3 class="page-title">Add Hosting Package</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('hostingcategory.index') }}">Hosting Categories</a></li>
                        <li class="breadcrumb-item active">Add Hosting Package</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">

                        <form method="POST" action="{{ route('hostingcategory.store') }}">
                            @csrf
                            <div class="row">
                                <div class="col-12 col-sm-6 col-md-6">
                                    <div class="form-group local-forms">
                                        <label>Hosting Package Name <span class="login-danger">*</span></label>
                                        <input type="text" class="form-control @error('name')is-invalid @enderror"
                                            name="name" value="{{ old('name') }}" placeholder="Hosting Package Name">
                                        @error('name')
                                            <p style="color: indianred;margin-top: 5px">{{ $message }}</p>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-6">
                                    <div class="form-group local-forms">
                                        <label>Hosting Provider Name <span class="login-danger">*</span></label>
                                        <input type="text" class="form-control @error('provider')is-invalid @enderror"
                                            name="provider" value="{{ old('provider') }}"
                                            placeholder="Hosting Provider Name">
                                        @error('provider')
                                            <p style="color: indianred;margin-top: 5px">{{ $message }}</p>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-4">
                                    <div class="form-group local-forms">
                                        <label>Hosting Cost Price <span class="login-danger">*</span></label>
                                        <input type="number" class="form-control @error('price')is-invalid @enderror"
                                            name="price" value="{{ old('price') }}" placeholder="Hosting Price">
                                        @error('price')
                                            <p style="color: indianred;margin-top: 5px">{{ $message }}</p>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-12 col-sm-6 col-md-4">
                                    <div class="form-group local-forms">
                                        <label>Hosting Issue Date <span class="login-danger">*</span></label>
                                        <input class="form-control datetimepicker" type="date" placeholder="DD-MM-YYYY"
                                            name="issue_date" value="{{ old('issue_date') }}">
                                        @error('issue_date')
                                            <p style="color: indianred;margin-top: 5px">{{ $message }}</p>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-4">
                                    <div class="form-group local-forms">
                                        <label>Hosting Expiry Date<span class="login-danger">*</span></label>
                                        <input type="date" class="form-control @error('expiry_date')is-invalid @enderror"
                                            name="expiry_date" value="{{ old('expiry_date') }}"
                                            placeholder="Domain expiry date">
                                        @error('expiry_date')
                                            <p style="color: indianred;margin-top: 5px">{{ $message }}</p>
                                        @enderror
                                    </div>

                                </div>
                                <div class="col-12">
                                    <div class="student-submit">
                                        <button type="submit" class="btn btn-primary">Save Hosting Category</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
