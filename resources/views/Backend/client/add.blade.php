@extends('Backend.dashboard.main')
@section('title', 'Client Add')
@section('content')
    <div class="content container-fluid">

        <div class="page-header">
            <div class="row align-items-center">
                <div class="col">
                    <h3 class="page-title">Add Client</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('client.index') }}">Clients</a></li>
                        <li class="breadcrumb-item active">Add Client</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <a class="btn btn-primary float-end mb-4" title="add more client" id="addmore">+</a>
                <div class="card">

                    <div class="card-body">
                        @if($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>

                        @endif
                        <form method="POST" action="{{ route('client.store') }}">
                            @csrf
                            <div class="input-fields">
                                <div class="row">

                                    <div class="col-12 col-sm-6 col-md-3">
                                        <div class="form-group local-forms">
                                            <label>Client Name <span class="login-danger">*</span></label>
                                            <input type="text" class="form-control @error('name')is-invalid @enderror"
                                                name="inputs[0][name]" value="{{ old('name') }}" placeholder="Client Name">
                                            @error('name')
                                                <p style="color: indianred;margin-top: 5px">{{ $message }}</p>
                                            @enderror
                                        </div>

                                    </div>
                                    <div class="col-12 col-sm-6 col-md-3">
                                        <div class="form-group local-forms">
                                            <label>Client Email <span class="login-danger">*</span></label>
                                            <input type="email" class="form-control @error('email')is-invalid @enderror"
                                                name="inputs[0][email]" value="{{ old('email') }}" placeholder="Client email">
                                            @error('email')
                                                <p style="color: indianred;margin-top: 5px">{{ $message }}</p>
                                            @enderror
                                        </div>

                                    </div>
                                    <div class="col-12 col-sm-6 col-md-3">
                                        <div class="form-group local-forms">
                                            <label>Client Phone Number <span class="login-danger">*</span></label>
                                            <input type="text" class="form-control @error('phone')is-invalid @enderror"
                                                name="inputs[0][phone]" value="{{ old('phone') }}" placeholder="Client phone">
                                            @error('phone')
                                                <p style="color: indianred;margin-top: 5px">{{ $message }}</p>
                                            @enderror
                                        </div>

                                    </div>
                                    <div class="col-12 col-sm-6 col-md-3">
                                        <div class="form-group local-forms">
                                            <label>Client Address</label>
                                            <input type="text" class="form-control @error('address')is-invalid @enderror"
                                                name="inputs[0][address]" value="{{ old('address') }}" placeholder="Client address">
                                            @error('address')
                                                <p style="color: indianred;margin-top: 5px">{{ $message }}</p>
                                            @enderror
                                        </div>

                                    </div>


                                </div>

                            </div>
                            <div class="col-12">
                                <div class="student-submit">
                                    <button type="submit" class="btn btn-primary">Save Client</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script>
    $(document).ready(function(){
        var max_fields = 10; // maximum input fields allowed
            var wrapper = $(".input-fields"); // container for input fields
            var add_button = $("#addmore"); // add more button

            var x = 1; // initialize counter for input fields

            $(add_button).click(function(e){
                e.preventDefault();
                x++;
                $(wrapper).append(
                    `<div class="row my-fields">

                        <div class="col-12 col-sm-6 col-md-3">
                            <div class="form-group local-forms">
                                <label>Client Name <span class="login-danger">*</span></label>
                                <input type="text" class="form-control @error('name')is-invalid @enderror"
                                    name="inputs[`+x+`][name]" value="{{ old('name') }}" placeholder="Client Name">
                                @error('name')
                                    <p style="color: indianred;margin-top: 5px">{{ $message }}</p>
                                @enderror
                            </div>

                        </div>
                        <div class="col-12 col-sm-6 col-md-3">
                            <div class="form-group local-forms">
                                <label>Client Email <span class="login-danger">*</span></label>
                                <input type="email" class="form-control @error('email')is-invalid @enderror"
                                    name="inputs[`+x+`][email]" value="{{ old('email') }}" placeholder="Client email">
                                @error('email')
                                    <p style="color: indianred;margin-top: 5px">{{ $message }}</p>
                                @enderror
                            </div>

                        </div>
                        <div class="col-12 col-sm-6 col-md-3">
                            <div class="form-group local-forms">
                                <label>Client Phone Number <span class="login-danger">*</span></label>
                                <input type="text" class="form-control @error('phone')is-invalid @enderror"
                                    name="inputs[`+x+`][phone]" value="{{ old('phone') }}" placeholder="Client phone">
                                @error('phone')
                                    <p style="color: indianred;margin-top: 5px">{{ $message }}</p>
                                @enderror
                            </div>

                        </div>
                        <div class="col-12 col-sm-6 col-md-2">
                            <div class="form-group local-forms">
                                <label>Client Address</label>
                                <input type="text" class="form-control @error('address')is-invalid @enderror"
                                    name="inputs[`+x+`][address]" value="{{ old('address') }}" placeholder="Client address">
                                @error('address')
                                    <p style="color: indianred;margin-top: 5px">{{ $message }}</p>
                                @enderror
                            </div>

                        </div>

                        <div class="col-12 col-md-1">
                        <a class="btn btn-danger remove-btn" title="remove client">x</a>
                        </div>
                    </tr>
                    </div>`
                )

            });
    });
    $(document).on('click','.remove-btn',function(){
        $(this).parents('.my-fields').remove();
    });

</script>
@endsection
