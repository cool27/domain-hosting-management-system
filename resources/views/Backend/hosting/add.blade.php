@extends('Backend.dashboard.main')
@section('title', 'Hosting Add')
@section('content')
    <div class="content container-fluid">

        <div class="page-header">
            <div class="row align-items-center">
                <div class="col">
                    <h3 class="page-title">Add Hosting Details</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('hosting.index') }}">Hostings</a></li>
                        <li class="breadcrumb-item active">Add Hosting Details</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">

                        <form method="POST" action="{{ route('hosting.store') }}">
                            @csrf
                            <div class="row">
                                <div class="col-12 col-sm-6 col-md-6">
                                    <div class="form-group" data-select2-id="11">

                                        <select class="select select2-hidden-accessible" name="client_id"
                                            data-select2-id="1" tabindex="-1" aria-hidden="true">
                                            <option value="" selected disabled>-- choose client -- </option>
                                            @if ($client->isNotEmpty())
                                                @foreach ($client as $key => $value)
                                                    <option value="{{ $value }}"
                                                        {{ old('client_id') == $value ? 'selected' : '' }}>
                                                        {{ $key }}
                                                    </option>
                                                @endforeach
                                            @endif


                                        </select>
                                        @error('client_id')
                                            <p style="color: indianred;margin-top: 5px">{{ $message }}</p>
                                        @enderror
                                    </div>

                                </div>

                                <div class="col-12 col-sm-6 col-md-6">
                                    <div class="form-group">

                                        <select class="form-control select" name="hosting_cat_id" id="exampleFormControlSelect1">
                                            <option>-- choose hosting type -- </option>
                                            @if ($hosting_cat)
                                                @foreach ($hosting_cat as $key => $value)
                                                    <option value="{{ $value->id }}" {{ old('hosting_cat_id') == $value->id ? 'selected' : '' }}>
                                                        {{ $value->name }}-{{ $value->provider }}</option>
                                                @endforeach
                                            @endif


                                        </select>
                                        @error('hosting_cat_id')
                                        <p style="color: indianred;margin-top: 5px">{{ $message }}</p>
                                    @enderror
                                    </div>

                                </div>
                                <div class="col-12 col-sm-12 col-md-12">
                                <div class="settings-form mt-0">

                                        <div class="form-group">
                                            <p class="pay-cont" style="font-weight:700">Is this an external domain?</p>
                                            <label class="custom_radio me-4">
                                            <input type="radio" name="external" id=""  value="no" checked="">
                                            <span class="checkmark"></span> No
                                            </label>
                                            <label class="custom_radio">
                                            <input type="radio" name="external" id="" value="yes">
                                            <span class="checkmark"></span> Yes
                                            </label>
                                            </div>

                                    </div>
                                </div>


                                <div class="col-12 col-sm-6 col-md-12" id="InternalDomain">
                                    <div class="form-group">

                                        <select class="form-control select"  name="domain_id" id="mySelectField">
                                            <option selected disabled>-- choose specific domain -- </option>
                                            @if ($domain)
                                                @foreach ($domain as $key => $value)
                                                    <option value="{{ $value }}" {{ old('domain_id') == $value ? 'selected' : '' }}>
                                                        {{ $key }}</option>
                                                @endforeach
                                            @endif


                                        </select>
                                        @error('domain_id')
                                        <p style="color: indianred;margin-top: 5px">{{ $message }}</p>
                                    @enderror
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-12" id="ExternalDomain">
                                    <div class="form-group local-forms">
                                        <label>Domain Name <span class="login-danger">*</span></label>
                                        <input class="form-control datetimepicker" type="text"
                                            placeholder="paste domain name eg. (https://www.google.com/)" name="external_domain"
                                            value="{{ old('external_domain') }}">
                                        @error('external_domain')
                                            <p style="color: indianred;margin-top: 5px">{{ $message }}</p>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-12 col-sm-6 col-md-4">
                                    <div class="form-group local-forms">
                                        <label>Hosting Issue Date <span class="login-danger">*</span></label>
                                        <input class="form-control datetimepicker" type="date" placeholder="DD-MM-YYYY"
                                            name="issue_date" value="{{ old('issue_date') }}">
                                        @error('issue_date')
                                            <p style="color: indianred;margin-top: 5px">{{ $message }}</p>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-4">
                                    <div class="form-group local-forms">
                                        <label>Hosting Expiry Date<span class="login-danger">*</span></label>
                                        <input type="date" class="form-control @error('expiry_date')is-invalid @enderror"
                                            name="expiry_date" value="{{ old('expiry_date') }}"
                                            placeholder="Domain expiry date">
                                        @error('expiry_date')
                                            <p style="color: indianred;margin-top: 5px">{{ $message }}</p>
                                        @enderror
                                    </div>

                                </div>
                                <div class="col-12 col-sm-6 col-md-4" id="selling_price">
                                    <div class="form-group local-forms">
                                        <label>Hosting Selling Price <span class="login-danger">*</span></label>
                                        <input class="form-control @error('selling_price')is-invalid @enderror" type="text"
                                            placeholder="selling price" name="selling_price"
                                            value="{{ old('selling_price') }}">
                                        @error('selling_price')
                                            <p style="color: indianred;margin-top: 5px">{{ $message }}</p>
                                        @enderror
                                    </div>
                                </div>


                                <div class="col-12">
                                    <div class="student-submit">
                                        <button type="submit" class="btn btn-primary">Save Hosting</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script>
    $(document).ready(function(){
        $('#ExternalDomain').hide();
        $("input[type='radio']").click(function(){
            var radioValue = $("input[name='external']:checked").val();
            if(radioValue=='yes')
            {
                $('#ExternalDomain').show();
                $('#InternalDomain').hide();
                $("input[name='external_domain']").val('')
            }
            else if(radioValue=='no')
            {
                $('#ExternalDomain').hide();
                $('#InternalDomain').show();
                //$('#mySelectField').val($('#mySelectField option:first').val());
            }
        });
    });
</script>
@endsection
