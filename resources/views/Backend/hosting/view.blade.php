@extends('Backend.dashboard.main')
@section('title', 'Hosting View')
@section('content')
    <div class="content container-fluid">

        <div class="page-header">
            <div class="row align-items-center">
                <div class="col">
                    <h3 class="page-title">Hosting Details</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Hosting Details</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card card-table">
                    <div class="card-body">

                        <div class="page-header">
                            <div class="row align-items-center">
                                <div class="col">
                                    <h3 class="page-title">Listed Hosting Details</h3>
                                </div>
                                <div class="col-auto text-end float-end ms-auto download-grp">

                                    <a href="{{ route('hosting.create') }}" title="add new hosting"
                                        class="btn btn-primary"><i class="fas fa-plus"></i>&nbsp;add new hosting</a>
                                </div>
                            </div>
                        </div>

                        <table class="table border-0 star-student table-hover table-center mb-0 datatable table-striped">
                            <thead class="student-thread">
                                <tr>
                                    <th width="">ID</th>
                                    <th>Client</th>
                                    <th width="">hosting Category</th>
                                    <th>Domain Name</th>
                                    <th>Issue Date</th>
                                    <th>Expiry Date</th>
                                    <th>Selling Price</th>
                                    <th>Payment Amount</th>
                                    <th>Due Amount</th>
                                    <th>Payment Status</th>
                                    <th class="text-end">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if ($hosting->isNotEmpty())
                                    @foreach ($hosting as $key => $value)
                                        <tr>

                                            <td>{{ $key + 1 }}</td>
                                            <td>{{ $value->client->name }}</td>
                                            <td class=""> <span
                                                    class="badge badge-success">{{ $value->hosting_cat->name }}-{{ $value->hosting_cat->provider }}</span>
                                            </td>
                                            @if ($value->domain == '')
                                                <td> <a href="{{ $value->external_domain }}" target="_blank">
                                                        {{ $value->external_domain }}<span
                                                            class="text-success">(External)</span></a>

                                                </td>
                                            @else
                                                <td> <a href="{{ $value->domain->name }}" class="link-success"
                                                        target="_blank">
                                                        {{ $value->domain->name }}</a></td>
                                            @endif
                                            @php
                                                $expiryDate = strtotime($value->expiry_date);
                                                $currentTime = time();
                                                $secondsRemaining = $expiryDate - $currentTime;
                                                $daysRemaining = floor($secondsRemaining / 86400);
                                                $finalcount = $daysRemaining + 1;
                                            @endphp

                                            <td>{{ $value->issue_date }}</td>

                                            <td>{{ $value->expiry_date }}
                                                @if ($finalcount == 0)
                                                    <span class="badge badge-primary">Expired

                                                    </span>
                                                @endif
                                                @if ($finalcount < 15 && $finalcount != 0)
                                                    <span class="badge badge-danger">{{ $finalcount . ' days left' }}

                                                    </span>
                                                @elseif($finalcount != 0)
                                                    <span class="badge badge-info">{{ $finalcount . ' days left' }}
                                                @endif

                                                </span>
                                            </td>
                                            <td> <span class="badge badge-primary">Nrs. {{ $value->selling_price }}</span>
                                            </td>
                                            <td> <span class="text-primary">Nrs.
                                                    @if ($value->payment != '')
                                                        {{ $value->payment->payment_amount }}
                                                    @else
                                                        0
                                                    @endif
                                                </span>
                                            </td>
                                            <td> <span class="text-info">Nrs.
                                                    @if ($value->payment != '')
                                                        {{ $value->payment->due_amount }}
                                                    @else
                                                        0
                                                    @endif
                                                </span>
                                            </td>
                                            @if ($value->payment && $value->payment->payment_status == 'full')
                                                <td class="text-success"> <span class="badge badge-success">FullyPaid</span>
                                                </td>
                                            @elseif ($value->payment && $value->payment->payment_status == 'partial')
                                                <td class="text-success"> <span
                                                        class="badge badge-info">PartiallyPaid</span>
                                                </td>
                                            @else
                                                <td class="text-success"> <span class="badge badge-danger">Payment
                                                        Due</span>
                                                </td>
                                            @endif

                                            <td class="text-end">
                                                <div class="dropdown dropdown-action">
                                                    <a href="" class="action-icon dropdown-toggle"
                                                        data-bs-toggle="dropdown" aria-expanded="false"><i
                                                            class="fas fa-ellipsis-v"></i></a>
                                                    <div class="dropdown-menu dropdown-menu-end">
                                                        <a class="dropdown-item"
                                                            href="{{ route('hosting.edit', $value->id) }}"><i
                                                                class="far fa-edit me-2"></i>Edit</a>
                                                        <a class="dropdown-item" data-bs-toggle="modal"
                                                            data-bs-target="#bs-example-modal-lg{{ $value->id }}"><i
                                                                class="far fa-eye me-2"></i>View Invoice</a>


                                                        <a class="dropdown-item"
                                                            href="{{ route('view.invoice', $value->id) }}"
                                                            target="_blank"><i class="fas fa-print me-2"></i>Print
                                                            Invoice</a>
                                                        <button type="submit" id="confirm-text-{{ $value->id }}"
                                                            data-id="{{ $value->id }}"
                                                            class="dropdown-item delete-button">
                                                            <i class="feather-trash"></i>&nbsp; &nbsp;Delete
                                                        </button>
                                                        <a class="dropdown-item" href="javascript:void(0);"><i
                                                                class="far fa-paper-plane me-2"></i>Send Invoice</a>
                                                        @if ($value->payment && $value->payment->payment_status == 'full')
                                                            <a class="text-success dropdown-item"> <i
                                                                    class="fa fa-check"></i> Payment Made</a>
                                                        @else
                                                            <a class="dropdown-item" data-bs-toggle="modal"
                                                                data-bs-target="#bank_details{{ $value->id }}"
                                                                href="javascript:void(0);"><i
                                                                    class="far fa-copy me-2"></i>Make
                                                                Payment</a>
                                                        @endif

                                                        {{-- make payment modal --}}

                                                    </div>
                                                </div>
                                            </td>
                    </div>

                    </tr>
                    {{-- make payment modal --}}

                    <div class="modal custom-modal fade bank-details" id="bank_details{{ $value->id }}" role="dialog">
                        <div class="modal-dialog modal-dialog-centered modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <div class="form-header text-start mb-0">
                                        <h4 class="mb-0">Add Payment Details for {{ $value->client->name }}</h4>

                                        {{-- <h6><span>Hosting Package:</span>{{ $value->hosting_cat->name }}</h6>
                                        <h6><span>Domain Name:</span>{{ $value->domain->name }}</h6> --}}
                                    </div>
                                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>

                                </div>
                                <div class="modal-body">

                                    <div class="bank-inner-details">
                                        <div class="row">

                                            <div class="col-lg-4 col-md-4">
                                                <h6 class="">
                                                    Package:{{ $value->hosting_cat->name }}-{{ $value->hosting_cat->provider }}
                                                </h6>
                                            </div>
                                            <div class="col-lg-4 col-md-4">
                                                <h6>Domain:{{ $value->domain->name }}</h6>
                                            </div>
                                            <div class="col-lg-4 col-md-4">
                                                @if ($value->status == 'active')
                                                    <h6>Total Amount: <span
                                                            id="due_amount">{{ $value->selling_price }}</span>
                                                    </h6>
                                                    @if ($value->payment)
                                                        <ul>

                                                            <li><span>previously
                                                                    paid:</span>{{ $value->payment->payment_amount }}</li>
                                                            <li><span>Amount Left
                                                                </span>{{ $value->payment->due_amount }}</li>


                                                        </ul>
                                                    @endif
                                                @else
                                                    <h6 class="text-success"> <i class="fa fa-check"></i> Payment
                                                        Already
                                                        Made</h6>
                                                @endif

                                            </div>
                                            <form method="post"
                                                @if ($value->payment) action="{{ route('update-payment', $value->payment->id) }}"
                                                @else
                                                action="{{ route('make-payment') }}" @endif>
                                                @csrf
                                                <input type="hidden" name="hosting_id" id="hosting_id"
                                                    value="{{ $value->id }}">
                                                <div class="col-lg-6 col-md-6">
                                                    <div class="form-group">
                                                        <label>Payment Amount (NPR)</label>
                                                        <input type="text" name="payment_amount"
                                                            class="form-control payment-amount"
                                                            placeholder="Enter Payment Amount">
                                                    </div>
                                                    <div id="payment-errors" class="text-danger payment-errors"></div>
                                                </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <div class="bank-details-btn">
                                        <a href="javascript:void(0);" data-bs-dismiss="modal"
                                            class="btn bank-cancel-btn me-2">Cancel</a>
                                        <button class="btn bank-save-btn" type="submit" class="submit-button"
                                            id="submit-button"> <span class="normalpayment" id="normalpayment">Make
                                                Payment</span> <span class="beforeSend" id="beforeSend"> <i
                                                    class="fas fa-spinner fa-pulse"></i> Making
                                                Payment</span></button>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    @endif
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection

<div class="modal fade" id="bs-example-modal-lg{{ $value->id }}" tabindex="-1" role="dialog"
    aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myLargeModalLabel">Invoice
                    Details</h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="">
                    <div class="content container-fluid">
                        <div class="row justify-content-center">
                            <div class="col-xl-12">
                                <div class="card invoice-info-card">
                                    <div class="card-body">
                                        <div class="invoice-item invoice-item-one">
                                            <div class="row">
                                                <div class="col-md-6" style="text-align: left">
                                                    <div class="invoice-logo">
                                                        <img src="{{ asset('Backend/assets/img/logo.png') }}"
                                                            alt="logo">
                                                    </div>
                                                    <div class="invoice-head">
                                                        <h2>Invoice
                                                            Number</h2>
                                                        <p>

                                                            In983248782
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="invoice-info">
                                                        <strong class="customer-text-one">Invoice
                                                            From</strong>
                                                        <h6 class="invoice-name">
                                                            Company Name
                                                        </h6>
                                                        <p class="invoice-details">
                                                            9087484288
                                                            <br>
                                                            Address line
                                                            1, Address
                                                            line 2<br>
                                                            Zip code
                                                            ,City -
                                                            Country
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="invoice-item invoice-item-two">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="invoice-info">
                                                        <strong class="customer-text-one"
                                                            style="text-align: left">Billed
                                                            to:
                                                            {{ $value->client->name }}</strong>

                                                        <p class="invoice-details invoice-details-two">
                                                            {{ $value->client->phone }}
                                                            <br>
                                                            {{ $value->client->address }}
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="invoice-info invoice-info2">
                                                        <strong class="customer-text-one">Payment
                                                            Status</strong>
                                                        <p class="invoice-details badge badge-danger text-white">
                                                            {{ $value->status == 'active' ? 'UnPaid' : '' }}
                                                        </p>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        {{-- <div
                                            class="invoice-issues-box">
                                            <div class="row">
                                                <div
                                                    class="col-lg-4 col-md-4">
                                                    <div
                                                        class="invoice-issues-date">
                                                        <p>Issue Date :
                                                            27 Jul 2022
                                                        </p>
                                                    </div>
                                                </div>
                                                <div
                                                    class="col-lg-4 col-md-4">
                                                    <div
                                                        class="invoice-issues-date">
                                                        <p>Due Date : 27
                                                            Aug 2022</p>
                                                    </div>
                                                </div>
                                                <div
                                                    class="col-lg-4 col-md-4">
                                                    <div
                                                        class="invoice-issues-date">
                                                        <p>Due Amount :
                                                            ₹ 1,54,22
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> --}}


                                        <div class="invoice-item invoice-table-wrap">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="table-responsive">
                                                        <table class="invoice-table table table-center mb-0">
                                                            <thead>
                                                                <tr>
                                                                    <th class="text-start">
                                                                        Package

                                                                    </th>
                                                                    {{-- <th>
                                                                        Domain
                                                                    </th> --}}
                                                                    <th>Validity
                                                                    </th>

                                                                    <th class="text-end">
                                                                        Amount
                                                                    </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td class="text-start">
                                                                        {{ $value->hosting_cat->name }}-{{ $value->hosting_cat->provider }}
                                                                        <br>
                                                                        {{ $value->domain->name }}
                                                                    </td>
                                                                    {{-- <td>{{ $value->domain->name }}
                                                                    </td> --}}
                                                                    <td>
                                                                        {{ $value->issue_date }}
                                                                        -
                                                                        {{ $value->expiry_date }}
                                                                    </td>
                                                                    <td>
                                                                        NRs.
                                                                        {{ $value->selling_price }}
                                                                    </td>

                                                                </tr>


                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row align-items-center justify-content-center text-start">
                                            <div class="col-lg-6 col-md-6">
                                                <div class="invoice-terms">
                                                    <h6>Support:</h6>
                                                    <p class="mb-0">
                                                        015560093/9860036615
                                                    </p>
                                                </div>

                                            </div>
                                            <div class="col-lg-6 col-md-6">
                                                <div class="invoice-total-card">
                                                    <div class="invoice-total-box">
                                                        <div class="invoice-total-inner">
                                                            <p>Taxable
                                                                <span>{{ $subtotal = $value->selling_price }}</span>
                                                            </p>

                                                            <p>Value
                                                                Added
                                                                Tax(13%)
                                                                <span>{{ $tax = (13 / 100) * $value->selling_price }}</span>
                                                            </p>

                                                            <p class="mb-0">
                                                                Sub
                                                                total
                                                                <span>{{ $subtotal + $tax }}</span>
                                                            </p>
                                                        </div>
                                                        <div class="invoice-total-footer">
                                                            <h4>Total
                                                                Amount
                                                                <span>$143,300.00</span>
                                                            </h4>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="invoice-sign text-end">
                                            <img class="img-fluid d-inline-block"
                                                src="{{ asset('Backend/assets/img/signature.png') }}" alt="sign">
                                            <span class="d-block">Harristemp</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@section('scripts')
    <script>
        $(document).ready(function() {

            $(".delete-button").on("click", function() {
                var id = $(this).data("id");
                //alert(id);
                Swal.fire({
                    title: "Are you sure?",
                    text: "You won't be able to revert this!",
                    type: "warning",
                    showCancelButton: !0,
                    confirmButtonColor: "#3085d6",
                    cancelButtonColor: "#d33",
                    confirmButtonText: "Yes, delete it!",
                    confirmButtonClass: "btn btn-primary",
                    cancelButtonClass: "btn btn-danger ml-1",
                    buttonsStyling: !1,
                }).then(function(t) {
                    if (t.value) {
                        // Send AJAX request to delete.php
                        $.ajax({
                            url: "{{ route('myhosting.destroy', ':id') }}".replace(':id',
                                id),
                            method: "POST",
                            data: {
                                _token: "{{ csrf_token() }}",
                                id: id
                            },
                            success: function(response) {
                                Swal.fire({
                                    type: "success",
                                    title: "Deleted!",
                                    text: response.message,
                                    confirmButtonClass: "btn btn-success"
                                }).then(function() {
                                    location.reload();
                                });
                            },
                            error: function(xhr) {
                                Swal.fire({
                                    type: "error",
                                    title: "Oops...",
                                    text: xhr.responseJSON.message
                                });
                            }
                        });
                    }
                });
            });
            $("#confirm-color").on("click", function() {

                Swal.fire({
                    title: "Are you sure?",
                    text: "You won't be able to revert this!",
                    type: "warning",
                    showCancelButton: !0,
                    confirmButtonColor: "#3085d6",
                    cancelButtonColor: "#d33",
                    confirmButtonText: "Yes, delete it!",
                    confirmButtonClass: "btn btn-primary",
                    cancelButtonClass: "btn btn-danger ml-1",
                    buttonsStyling: !1,
                }).then(function(t) {
                    t.value ?
                        Swal.fire({
                            type: "success",
                            title: "Deleted!",
                            text: "Your file has been deleted.",
                            confirmButtonClass: "btn btn-success"
                        }) :
                        t.dismiss === Swal.DismissReason.cancel && Swal.fire({
                            title: "Cancelled",
                            text: "Your imaginary file is safe :)",
                            type: "error",
                            confirmButtonClass: "btn btn-success"
                        });
                });
            });
        });
    </script>
    {{-- 
    <script>
        $(document).ready(function() {
            $('.beforeSend').hide();
            $('.normalButton').show();
            $('#success-message').hide();
            var radioFull = $("input[name='payment'][value='full']");
            var radioPartial = $("input[name='payment'][value='partial']");

            // hide DueAmount field and disable paymentAmountInput initially
            $(".due-amount").prop('disabled', true).parent().hide();
            $(".payment-amount").prop('disabled', true);

            // show/hide DueAmount field and enable/disable paymentAmountInput when radio buttons clicked
            $("input[name='payment']").click(function() {
                var paymentAmountInput = $(this).parents('.modal-body').find('.payment-amount');
                var dueAmountInput = $(this).parents('.modal-body').find('.due-amount');
                var billableAmount = $(this).parents('.modal-body').find('#due_amount')
                    .text(); // convert to float

                if (radioFull.is(':checked')) {
                    dueAmountInput.prop('disabled', true).parent().hide();
                    paymentAmountInput.prop('disabled', true).val(billableAmount);
                    var HostingId = $(this).parents('.modal-body').find('#hosting_id').val();


                    $('#submit-button' + HostingId).off('click').on('click', function(e) {
                        e.preventDefault();
                        var paymentAmount = parseFloat(paymentAmountInput.val());


                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': '{{ csrf_token() }}'
                            },
                            type: "POST",
                            url: '{{ route('make-payment') }}',
                            data: {
                                Paymenttype: 'full',
                                PaymentAmount: paymentAmount,


                                HostingId: HostingId
                            },
                            beforeSend: function() {
                                $('.beforeSend').show();
                                $('.normalButton').hide();


                            },
                            success: function(response) {
                                console.log(response);
                                $('.beforeSend').hide();
                                $('.normalButton').show();
                                $('#success-message').show();
                            },
                            error: function(xhr, status, error) {
                                console.log(error);
                            }
                        });
                    });
                } else if (radioPartial.is(':checked')) {
                    dueAmountInput.prop('disabled', true).parent().show().attr('placeholder', 'Due amount');
                    paymentAmountInput.prop('disabled', false).val('').attr('placeholder',
                        'Enter Partial Payment');
                    dueAmountInput.val('');
                    var HostingId = $(this).parents('.modal-body').find('#hosting_id').val();

                    // calculate due amount when payment amount entered
                    $(".payment-amount").off('input').on('input', function() {
                        var paymentAmountInput = $(this);
                        var dueAmountInput = $(this).parents('.modal-body').find('.due-amount');
                        var billableAmount = $(this).parents('.modal-body').find('#due_amount')
                            .text(); // convert to float

                        var paymentAmount = parseFloat(paymentAmountInput.val());
                        var HostingId = $(this).parents('.modal-body').find('#hosting_id').val();

                        if (!isNaN(paymentAmount) && paymentAmount <= billableAmount) {
                            var dueAmount = billableAmount - paymentAmount;
                            dueAmountInput.val(dueAmount.toFixed(2));

                        } else if (isNaN(paymentAmount)) {
                            dueAmountInput.val(billableAmount);
                        } else {
                            dueAmountInput.val('payment amount exceedes');
                        }
                    });

                    $('#submit-button' + HostingId).off('click').on('click', function(e) {
                        e.preventDefault();
                        var paymentAmount = parseFloat(paymentAmountInput.val());
                        var dueAmount = parseFloat(dueAmountInput.val());

                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': '{{ csrf_token() }}'
                            },
                            type: "POST",
                            url: '{{ route('make-payment') }}',
                            data: {
                                Paymenttype: 'partial',
                                DueAmount: dueAmount,
                                PaymentAmount: paymentAmount,
                                HostingId: HostingId
                            },
                            beforeSend: function() {
                                $('.beforeSend').show();
                                $('.normalButton').hide();


                            },
                            success: function(response) {
                                console.log(response);
                                $('.beforeSend').hide();
                                $('.normalButton').show();
                                $('#success-message').show();
                            },
                            error: function(xhr, status, error) {
                                console.log(error);
                            }
                        });
                    });
                }
            });
        });
    </script> --}}
    <script>
        $('.beforeSend').hide();
        $('.normalpayment').show();
        $(document).on('submit', 'form', function(e) {

            e.preventDefault();
            var form = $(this);
            var url = form.attr('action');
            var method = form.attr('method');
            var data = form.serialize();

            // Make an Ajax request to the server
            $.ajax({
                url: url,
                type: method,
                data: data,

                beforeSend: function() {
                    $('.beforeSend').show();
                    $('.normalpayment').hide();


                },
                success: function(response) {
                    // If the server returns a success response, close the modal
                    $('.bank-details').modal('hide');
                    $('.beforeSend').hide();
                    $('.normalpayment').show();
                    location.reload();
                },
                error: function(response) {
                    var errors = response.responseJSON.errors;
                    var fetchedError = errors.payment_amount;
                    // alert(fetchedError);
                    $('.payment-errors').text(fetchedError);
                    // // alert(errors.payment_amount);
                    // var errorHtml = '';
                    // $.each(errors, function(key, value) {
                    //     errorHtml += '<div class="alert alert-danger">' + value.payment_amount +
                    //         '</div>';
                    // });
                    // $('#bank_details .modal-body').prepend(errorHtml);
                }
            });
        });
    </script>
@endsection
