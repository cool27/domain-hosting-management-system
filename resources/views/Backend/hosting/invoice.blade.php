<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>{{ $hosting->client->name }} | Invoice</title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html2pdf.js/0.9.3/html2pdf.bundle.min.js"></script>

</head>

<body>
    <div id="divToExport">


        <header id="clearfix">
            <div id="logo">
                <img src="{{ asset('Backend/assets/img/logo.png') }}">
            </div>

            <div id="company" class="clearfix">
                <div>Company Name</div>
                <div>455 Foggy Heights,<br /> AZ 85004, US</div>
                <div>(602) 519-0450</div>
                <div><a href="mailto:company@example.com">company@example.com</a></div>
            </div>
            <div id="project">
                <div><span>Bill To::</span> {{ $hosting->client->name }}</div>
                <div><span>Phone</span> {{ $hosting->client->phone }}</div>
                <div><span>Address</span> {{ $hosting->client->address }}</div>
                <div><span>Email</span> <a href="mailto:{{ $hosting->client->email }}">{{ $hosting->client->email }}</a>
                </div>
                <div><span>Billing Date</span> {{ $hosting->created_at->format('d-m-Y') }}</div>

            </div>
        </header>
        <main style="">
            <table>
                <thead>
                    <tr>
                        <th class="service">Package</th>
                        <th class="service">Domain</th>

                        <th class="desc">Validity</th>
                        <th>Amount</th>

                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="service">{{ $hosting->hosting_cat->name }}</td>
                        <td class="service">{{ $hosting->domain->name }}</td>
                        <td class="desc">{{ $hosting->issue_date }}-{{ $hosting->expiry_date }}</td>
                        <td class="unit">Nrs.{{ $hosting->selling_price }}</td>

                    </tr>


                </tbody>
            </table>
            <div id="" style="float: right">
                <div><span>Taxable::</span> {{ $subtotal = $hosting->selling_price }}</div>
                <div><span>VAT (13%)::</span> {{ $tax = (13 / 100) * $hosting->selling_price }}</div>
                <hr>
                <div><span>Total::</span> {{ $subtotal + $tax }}</div>


            </div>
            <div id="notices" style="margin-top:100px">
                <div>NOTICE:</div>
                <div class="notice">A finance charge of 1.5% will be made on unpaid balances after 30 days.</div>
            </div>

            <div>
                <button
                    style="margin-top:30px; background:#00AFE9; border:none; color:#fff;cursor: pointer;padding:10px; width:100px"
                    id="printBtn" onclick="printContent()">print</button>
                <button
                    style="margin-top:30px; background:#00AFE9; border:none; color:#fff;cursor: pointer;padding:10px; width:100px"
                    id="downloadBtn" onclick="downloadPDF()">download</button>
            </div>
        </main>
        <footer>
            Invoice was created on a computer and is valid without the signature and seal.
        </footer>
    </div>
</body>

</html>

<style>
    /* .clearfix:after {
        content: "";
        display: table;
        clear: both;
    } */

    a {
        color: #5D6975;
        text-decoration: underline;
    }

    body {
        position: relative;
        width: 21cm;
        height: 29.7cm;
        margin: 0 auto;
        color: #001028;
        background: #FFFFFF;
        font-family: Arial, sans-serif;
        font-size: 14px;
        font-family: Arial;
    }

    header {
        padding: 10px 0;
        margin-bottom: 30px;
        margin-top: 50px;
    }

    #logo {
        text-align: center;
        margin-bottom: 10px;
    }

    #logo img {
        width: 200px;
    }

    h4 {
        border-top: 1px solid #5D6975;
        border-bottom: 1px solid #5D6975;
        color: #5D6975;
        font-size: 2.4em;
        line-height: 1.4em;
        font-weight: normal;
        text-align: center;
        margin: 0 0 20px 0;
        background: url(dimension.png);
    }

    #project {
        float: left;
    }

    #project span {
        color: #5D6975;
        text-align: right;
        width: 52px;
        margin-right: 10px;
        display: inline-block;
        font-size: 0.9em;
    }

    #company {
        float: right;
        text-align: right;
    }

    #project div,
    #company div {
        white-space: nowrap;
    }

    table {
        width: 100%;
        border-collapse: collapse;
        border-spacing: 0;
        margin-bottom: 20px;
        margin-top: 150px;
    }

    table tr:nth-child(2n-1) td {
        background: #F5F5F5;
    }

    table th,
    table td {
        text-align: center;
    }

    table th {
        padding: 5px 20px;
        color: #5D6975;
        border-bottom: 1px solid #C1CED9;
        white-space: nowrap;
        font-weight: normal;
    }

    table .service,
    table .desc {
        text-align: left;
    }

    table td {
        padding: 20px;
        text-align: right;
    }

    table td.service,
    table td.desc {
        vertical-align: top;
    }

    table td.unit,
    table td.qty,
    table td.total {
        font-size: 1.2em;
    }

    table td.grand {
        border-top: 1px solid #5D6975;
        ;
    }

    #notices .notice {
        color: #5D6975;
        font-size: 1.2em;
    }

    footer {
        color: #5D6975;
        width: 100%;
        height: 30px;
        position: absolute;
        bottom: 300px;
        border-top: 1px solid #C1CED9;
        padding: 8px 0;
        text-align: center;
    }
</style>
<script>
    function printContent() {
        var printButton = document.getElementById("printBtn");
        var downloadButton = document.getElementById("downloadBtn");
        printButton.style.display = "none"; // hide print button
        downloadButton.style.display = "none"; // hide print button
        window.print(); // print the page
        printButton.style.display = "block"; // show print button again
        downloadButton.style.display = "block"; // show print button again
    }

    function downloadPDF() {
        // Choose the element id which you want to export.
        var element = document.getElementById('divToExport');
        var printButton = document.getElementById("printBtn");
        var downloadButton = document.getElementById("downloadBtn");

        // Hide the download and print buttons.
        printButton.style.display = "none";
        downloadButton.style.display = "none";

        // Set the size of the element.
        element.style.width = '700px';
        element.style.height = '900px';

        var opt = {
            margin: 0.5,
            filename: 'hostingreceipt.pdf',
            image: {
                type: 'jpeg',
                quality: 1
            },
            html2canvas: {
                scale: 1
            },
            jsPDF: {
                unit: 'in',
                format: 'letter',
                orientation: 'portrait',
                precision: '12'
            }
        };

        // choose the element and pass it to html2pdf() function and call the save() on it to save as pdf.
        html2pdf().set(opt).from(element).save().then(() => {
            // Show the download and print buttons after PDF generation is complete.
            printButton.style.display = "block";
            downloadButton.style.display = "block";
        });
    }
</script>
