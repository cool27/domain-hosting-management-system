@extends('Backend.dashboard.main')
@section('content')
<div class="content container-fluid">

    <div class="page-header">
        <div class="row align-items-center">
            <div class="col">
                <h3 class="page-title">Add Class</h3>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="departments.html">Class</a></li>
                    <li class="breadcrumb-item active">Add Class</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <form method="POST" action="{{ route('class.store') }}">
                        @csrf
                        <div class="row">
                            <div class="col-12">
                                <h5 class="form-title"><span>Class Details</span></h5>
                            </div>
                            <div class="col-12 col-sm-4">
                                <div class="form-group local-forms">
                                    <label>Class <span class="login-danger">*</span></label>
                                    <input type="text" class="form-control @error('name')is-invalid @enderror" name="name" value="{{ old('name') }}">
                                    @error('name')
                                    <p style="color: indianred;margin-top: 5px">{{ $message }}</p>
                                    @enderror
                                </div>

                            </div>
                            <div class="col-12 col-sm-4">
                                <div class="form-group local-forms">
                                    <label>Started Year <span class="login-danger">*</span></label>
                                    <input type="number" class="form-control @error('started_year')is-invalid @enderror" name="started_year" value="{{ old('started_year') }}">
                                    @error('started_year')
                                    <p style="color: indianred;margin-top: 5px">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                            <h5 class="mb-4">Add Section of This Class</h5>
                        @foreach($sections as $section)
                        <div class="mb-2">
                            <input type="checkbox" name="sections[]" value="{{ $section->id }}">
                            <label>{{ $section->name }}</label>
                        </div>
                        @endforeach

                            <div class="col-12">
                                <div class="student-submit">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
