<!DOCTYPE html>
<html lang="en">

<head>
    @include('Backend.dashboard.header')
    @yield('styles')
</head>


<body>


    <div class="main-wrapper">

        @include('Backend.dashboard.top-nav')


        @include('Backend.dashboard.sidebar')
        <div class="page-wrapper">
            @yield('content')
            <footer>
                <p>Copyright ©
                    <script>
                        document.write(new Date().getFullYear())
                    </script> DHMS || Powered By <a href="https://communicate.com.np/"
                        target="_blank">Communicate</a>.
                </p>
            </footer>
        </div>
    </div>

    @include('Backend.dashboard.footer-scripts')


    @yield('scripts')
    @include('Backend.dashboard.toaster-message');


</body>

</html>
