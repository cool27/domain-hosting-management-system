@php
    $whouser = Auth::guard('admin')->user();
@endphp

<div class="sidebar" id="sidebar">
    <div class="sidebar-inner slimscroll">
        <div id="sidebar-menu" class="sidebar-menu">
            <ul>
                <li class="menu-title">
                    <span>Main Menu</span>
                </li>
                <li class=" @if (Request::routeIs('dashboard')) active @endif ">
                    <a href="{{ route('dashboard') }}"> <span> Dashboard</span> </a>

                </li>
                {{-- @if (
                    $whouser->can('role.create') ||
                        $whouser->can('role.edit') ||
                        $whouser->can('role.delete') ||
                        $whouser->can('role.view'))
                    <li class="submenu @if (Request::routeIs('roles.index', 'roles.create', 'roles.edit')) active @endif">
                        <a href="#"><i class="fas fa-tasks"></i> <span> Roles & Permissions</span> <span
                                class="menu-arrow"></span></a>
                        <ul>
                            @if ($whouser->can('role.view'))
                                <li><a href="{{ route('roles.index') }}"
                                        class="@if (Request::routeIs('roles.index', 'roles.create', 'roles.edit')) active @endif ">Roles List</a></li>
                            @endif


                        </ul>
                    </li>
                @endif --}}
                {{--  users list  --}}
                {{-- @if (
                    $whouser->can('admin.create') ||
                        $whouser->can('admin.edit') ||
                        $whouser->can('admin.delete') ||
                        $whouser->can('admin.view'))
                    <li class="submenu @if (Request::routeIs('admin.index', 'admin.create', 'admin.edit')) active @endif">
                        <a href="{{ route('admin.index') }}"><i class="	fas fa-user-cog"></i> <span> System Users</span>
                            <span class="menu-arrow"></span></a>
                        <ul>


                            @if ($whouser->can('admin.view'))
                                <li><a href="{{ route('admin.index') }}"
                                        class="@if (Request::routeIs('admin.index', 'admin.create', 'admin.edit')) active @endif">User Lists</a></li>
                            @endif



                        </ul>
                    </li>
                @endif --}}

                @if (
                    $whouser->can('client.create') ||
                        $whouser->can('client.edit') ||
                        $whouser->can('client.delete') ||
                        $whouser->can('client.view'))
                    <li class="submenu @if (Request::routeIs('client.index', 'client.create', 'client.edit')) active @endif">
                        <a href="{{ route('client.index') }}"><i class="fas fa-users"></i> <span> client</span> <span
                                class="menu-arrow"></span></a>
                        <ul>


                            @if ($whouser->can('client.view'))
                                <li><a href="{{ route('client.index') }}"
                                        class="@if (Request::routeIs('client.index')) active @endif">View</a></li>
                            @endif
                            @if ($whouser->can('client.create'))
                                <li><a href="{{ route('client.create') }}"
                                        class="@if (Request::routeIs('client.create')) active @endif">Add</a></li>
                            @endif



                        </ul>
                    </li>
                @endif
                @if (
                    $whouser->can('hostingcategory.create') ||
                        $whouser->can('hostingcategory.edit') ||
                        $whouser->can('hostingcategory.delete') ||
                        $whouser->can('hostingcategory.view'))
                    <li class="submenu @if (Request::routeIs('hostingcategory.index', 'hostingcategory.create', 'hostingcategory.edit')) active @endif">
                        <a href="{{ route('hostingcategory.index') }}"><i class="fas fa-server"></i> <span>Hosting
                                Package</span> <span class="menu-arrow"></span></a>
                        <ul>
                            @if ($whouser->can('hostingcategory.view'))
                                <li><a href="{{ route('hostingcategory.index') }}"
                                        class="@if (Request::routeIs('hostingcategory.index')) active @endif">View</a></li>
                            @endif
                            @if ($whouser->can('hostingcategory.create'))
                                <li><a href="{{ route('hostingcategory.create') }}"
                                        class="@if (Request::routeIs('hostingcategory.create')) active @endif">Add</a></li>
                            @endif
                        </ul>
                    </li>
                @endif

                @if (
                    $whouser->can('domain.create') ||
                        $whouser->can('domain.edit') ||
                        $whouser->can('domain.delete') ||
                        $whouser->can('domain.view'))
                    <li class="submenu @if (Request::routeIs('domain.index', 'domain.create', 'domain.edit')) active @endif">
                        <a href="{{ route('domain.index') }}"><i class="fas fa-globe"></i> <span> domain</span> <span
                                class="menu-arrow"></span></a>
                        <ul>


                            @if ($whouser->can('domain.view'))
                                <li><a href="{{ route('domain.index') }}"
                                        class="@if (Request::routeIs('domain.index')) active @endif">View</a></li>
                            @endif
                            @if ($whouser->can('domain.create'))
                                <li><a href="{{ route('domain.create') }}"
                                        class="@if (Request::routeIs('domain.create')) active @endif">Add</a></li>
                            @endif



                        </ul>
                    </li>
                @endif

                @if (
                    $whouser->can('hosting.create') ||
                        $whouser->can('hosting.edit') ||
                        $whouser->can('hosting.delete') ||
                        $whouser->can('hosting.view'))
                    <li class="submenu @if (Request::routeIs('hosting.index', 'hosting.create', 'hosting.edit')) active @endif">
                        <a href="{{ route('hosting.index') }}"><i class="fas fa-server"></i> <span> hosting</span>
                            <span class="menu-arrow"></span></a>
                        <ul>


                            @if ($whouser->can('hosting.view'))
                                <li><a href="{{ route('hosting.index') }}"
                                        class="@if (Request::routeIs('hosting.index')) active @endif">View</a></li>
                            @endif
                            @if ($whouser->can('hosting.create'))
                                <li><a href="{{ route('hosting.create') }}"
                                        class="@if (Request::routeIs('hosting.create')) active @endif">Sale</a></li>
                            @endif



                        </ul>
                    </li>
                    @if (
                        $whouser->can('setting.create') ||
                            $whouser->can('setting.edit') ||
                            $whouser->can('setting.delete') ||
                            $whouser->can('setting.view') ||
                            $whouser->can('admin.view') ||
                            $whouser->can('admin.edit') ||
                            $whouser->can('admin.delete')||
                            $whouser->can('admin.create') ||
                            $whouser->can('roles.create') ||
                            $whouser->can('roles.edit') ||
                            $whouser->can('roles.delete') ||
                            $whouser->can('roles.view'))

                        <li class="submenu @if (Request::routeIs('systemsetting.index', 'systemsetting.create', 'systemsetting.edit','admin.index', 'admin.create', 'admin.edit','roles.index', 'roles.create', 'roles.edit')) active @endif">
                            <a href="{{ route('systemsetting.index') }}"><i class="fas fa-user-cog"></i> <span>system
                                    setting</span> <span class="menu-arrow"></span></a>
                            <ul>


                                @if ($whouser->can('setting.view'))
                                    <li><a href="{{ route('systemsetting.index') }}"
                                            class="@if (Request::routeIs('systemsetting.index')) active @endif">View</a></li>
                                @endif
                                {{-- @if ($whouser->can('setting.create'))
                                    <li><a href="{{ route('systemsetting.create') }}"
                                            class="@if (Request::routeIs('systemsetting.create')) active @endif">Add</a></li>
                                @endif --}}
                                @if ($whouser->can('role.view'))
                                <li><a href="{{ route('roles.index') }}"
                                        class="@if (Request::routeIs('roles.index', 'roles.create', 'roles.edit')) active @endif ">Roles</a></li>
                            @endif
                            @if ($whouser->can('admin.view'))
                            <li><a href="{{ route('admin.index') }}"
                                    class="@if (Request::routeIs('admin.index', 'admin.create', 'admin.edit')) active @endif">Users</a></li>
                        @endif



                            </ul>
                        </li>
                    @endif
                @endif


            </ul>
        </div>
    </div>
</div>
