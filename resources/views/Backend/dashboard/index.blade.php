@extends('Backend.dashboard.main')
@section('title', 'Dashboard')
@section('content')
    <div class="content container-fluid">

        <div class="page-header">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-sub-header">
                        <h3 class="page-title">Welcome Admin
                        </h3>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
                            <li class="breadcrumb-item active">Dashboard</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-xl-3 col-sm-6 col-12 d-flex">
                <div class="card bg-comman w-100">
                    <div class="card-body">
                        <div class="db-widgets d-flex justify-content-between align-items-center">
                            <div class="db-info">
                                <h5>Total Clients</h5>
                                <h3>{{ $client }}</h3>
                            </div>
                            {{-- <div class="db-icon">
                                <img src="{{ asset('Backend/assets/img/icons/dash-icon-01.svg') }}" alt="Dashboard Icon">
                            </div> --}}
                            <a class="btn btn-success btn-sm" href="{{ route('client.index') }}">view all</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-sm-6 col-12 d-flex">
                <div class="card bg-comman w-100">
                    <div class="card-body">
                        <div class="db-widgets d-flex justify-content-between align-items-center">
                            <div class="db-info">
                                <h5>Total Domains</h5>
                                <h3>{{ $domain }}</h3>
                            </div>
                            <a class="btn btn-danger btn-sm" href="{{ route('domain.index') }}">view all</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-sm-6 col-12 d-flex">
                <div class="card bg-comman w-100">
                    <div class="card-body">
                        <div class="db-widgets d-flex justify-content-between align-items-center">
                            <div class="db-info">
                                <h5>Sold Hosting</h5>
                                <h3>{{ $hosting }}</h3>
                            </div>
                            <a class="btn btn-warning text-white btn-sm" href="{{ route('hosting.index') }}">view all</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-sm-6 col-12 d-flex">
                <div class="card bg-comman w-100">
                    <div class="card-body">
                        <div class="db-widgets d-flex justify-content-between align-items-center">
                            <div class="db-info">
                                <h5>Availaible Hosting Packages</h5>
                                <h3>{{ $hosting_category }}</h3>
                            </div>
                            <a class="btn btn-info text-white btn-sm" href="{{ route('hostingcategory.index') }}">view
                                all</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- 
        <div class="row">
            <div class="col-md-12 col-lg-6">

                <div class="card card-chart">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-6">
                                <h5 class="card-title">Overview</h5>
                            </div>
                            <div class="col-6">
                                <ul class="chart-list-out">
                                    <li><span class="circle-blue"></span>Teacher</li>
                                    <li><span class="circle-green"></span>Student</li>
                                    <li class="star-menus"><a href="javascript:;"><i class="fas fa-ellipsis-v"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div id="apexcharts-area"></div>
                    </div>
                </div>

            </div>
            <div class="col-md-12 col-lg-6">

                <div class="card card-chart">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-6">
                                <h5 class="card-title">Number of Students</h5>
                            </div>
                            <div class="col-6">
                                <ul class="chart-list-out">
                                    <li><span class="circle-blue"></span>Girls</li>
                                    <li><span class="circle-green"></span>Boys</li>
                                    <li class="star-menus"><a href="javascript:;"><i class="fas fa-ellipsis-v"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div id="bar"></div>
                    </div>
                </div>

            </div>
        </div> --}}
        <div class="row">

            @if ($expiring_domain->isEmpty())
            @else
                <div class="col-xl-6 d-flex">

                    <div class="card flex-fill student-space comman-shadow">
                        <div class="card-header d-flex align-items-center">
                            <h5 class="card-title">Domain Expiring Soon</h5>

                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table star-student table-hover table-center table-borderless table-striped">
                                    <thead class="thead-light">
                                        <tr>
                                            <th>Domain Name</th>
                                            <th class="text-center">Clients</th>
                                            <th class="text-center">Days Left</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        @foreach ($expiring_domain as $key => $value)
                                            <tr>
                                                <td class="">{{ $value->name }}</td>
                                                <td class="text-center">{{ $value->client->name }}</td>
                                                <td class="text-center">
                                                    @php
                                                        $expiryDate = strtotime($value->expiry_date);
                                                        $currentTime = time();
                                                        $secondsRemaining = $expiryDate - $currentTime;
                                                        $daysRemaining = floor($secondsRemaining / 86400);
                                                        $finalcount = $daysRemaining + 1;
                                                    @endphp
                                                    <div> <span class="badge badge-danger text-white">{{ $finalcount }}
                                                            Days
                                                            Left</span></div>
                                                </td>
                                            </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>
                            <a href="{{ route('domain.index') }}" class="btn btn-info btn-sm mt-2 mx-2 text-white">view all
                                Domains</a>
                        </div>
                    </div>

                </div>
            @endif
            @if ($expiring_hosting->isEmpty())
            
               @else
               <div class="col-xl-6 d-flex">

                <div class="card flex-fill comman-shadow">
                    <div class="card-header d-flex align-items-center">
                        <h5 class="card-title ">Hosting Expiring Soon </h5>
                    </div>
                    <div class="card-body">
                        <div class="activity-groups">

                            @foreach ($expiring_hosting as $key => $value)
                                <div class="activity-awards">

                                    <div class="award-list-outs">
                                        <h4>{{ $value->domain->name }}</h4>
                                        <h4>Client: {{ $value->client->name }},&nbsp; Hosting Package: <span
                                                class="text-info">{{ $value->hosting_cat->name }} -
                                                {{ $value->hosting_cat->provider }}</span>
                                        </h4>
                                    </div>
                                    <div class="award-time-list">
                                        @php
                                            $expiryDate = strtotime($value->expiry_date);
                                            $currentTime = time();
                                            $secondsRemaining = $expiryDate - $currentTime;
                                            $daysRemaining = floor($secondsRemaining / 86400);
                                            $finalcount = $daysRemaining + 1;
                                        @endphp
                                        <span class="badge badge-danger text-white">{{ $finalcount }} Days
                                            Left</span>
                                    </div>
                                </div>
                            @endforeach




                        </div>
                        <a href="{{ route('hosting.index') }}" class="btn btn-info btn-sm mt-2 mx-2 text-white">view
                            all hostings</a>
                    </div>
                </div>

            </div>
            @endif
        </div>
    @endsection
