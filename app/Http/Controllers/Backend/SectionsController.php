<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\SectionsRequest;
use App\Models\Classes;
use App\Repositories\Sections\SectionRepository;
use Illuminate\Http\Request;

class SectionsController extends Controller
{
    public $sectionRepository;
    public function __construct(SectionRepository $sectionRepository)
    {
        $this->sectionRepository = $sectionRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $section = $this->sectionRepository->getAllSection();
        //dd($section);
        return view('Backend.section.view', compact('section'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SectionsRequest $request)
    {
        $this->validate($request, $request->messages());
        $section = $this->sectionRepository->addSection($request->all());
        if ($section) {
            return redirect()->route('section.index')->with('success', 'Section Added Successfully');
        } else {
            return redirect()->route('section.create')->with('error', 'Error in Adding Section');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($sectionId)
    {
        $section = $this->sectionRepository->getAllSection();
        $SingleSection = $this->sectionRepository->getSingleSection($sectionId);
        //dd($section);
        return view('Backend.section.view', compact('SingleSection', 'section'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SectionsRequest $request, $sectionId)
    {
        $section = $this->sectionRepository->updateSection($request->all(), $sectionId);
        if ($section) {
            return redirect()->route('section.index')->with('success', 'Section Updated Successfully');;
        } else {
            return redirect()->route('section.index')->with('error', 'Error in Updating Section');;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($sectionId)
    {
        $section = $this->sectionRepository->deleteSection($sectionId);
        if ($section) {
            return redirect()->back()->with('success', 'Section Deleted Successfully');
        } else {
            return redirect()->back()->with('error', 'Error in Deleting Section');
        }
    }
}
