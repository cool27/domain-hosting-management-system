<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Client;
use App\Models\Domain;
use Illuminate\Http\Request;
use App\Rules\DomainName;
use App\Rules\ValidExpiryDate;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class DomainController extends Controller
{
    public $user;
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::guard('admin')->user();
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (is_null($this->user) || !$this->user->can('domain.view')) {
            abort(403, 'Sorry You are Unauthorized Access To View any Domain');
        }
        $domain = Domain::with('client')->get();
        //dd($domain);
        return view('Backend.domain.view', compact('domain'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (is_null($this->user) || !$this->user->can('domain.create')) {
            abort(403, 'Sorry You are Unauthorized Access To Add any Domain');
        }
        $client = Client::pluck('id', 'name');
        //dd($client);
        return view('Backend.domain.add', compact('client'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (is_null($this->user) || !$this->user->can('domain.create')) {
            abort(403, 'Sorry You are Unauthorized Access To Add any Domain');
        }
        $request->validate(
            [
                'name' => [
                    'required',
                    new DomainName,
                    Rule::unique('domains'),

                ],
                'issuer' => 'required',
                'issue_date' => 'required',
                'expiry_date' => ['required',new  ValidExpiryDate($request->all())],
                'client_id' => 'required',
                'selling_price' => 'required',
            ],
            ['name.required' => 'Please enter a domain name.',    'issuer.required' => 'Please enter domain issuer.',    'issue_date.required' => 'Please enter an issue date.',    'expiry_date.required' => 'Please enter an expiry date.',    'client_id.required' => 'Please choose a client.','selling_price'=>'please enter Selling Price']

        );
        $domain = new Domain();
        $domain->name = $request->name;
        $domain->issuer = $request->issuer;
        $domain->issue_date = $request->issue_date;
        $domain->expiry_date = $request->expiry_date;
        $domain->cost_price = $request->cost_price;
        $domain->selling_price = $request->selling_price;
        $domain->client_id = $request->client_id;
        $status = $domain->save();
        if ($status) {
            return redirect()->route('domain.index')->with('success', 'domain added successfully');
        } else {
            return redirect()->back()->with('error', 'problem in adding domain');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (is_null($this->user) || !$this->user->can('domain.edit')) {
            abort(403, 'Sorry You are Unauthorized Access To Edit any Domain');
        }
        $domain = Domain::find($id);
        $client = Client::pluck('id', 'name');
        //dd($domain);
        return view('Backend.domain.edit', compact('domain', 'client'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (is_null($this->user) || !$this->user->can('domain.edit')) {
            abort(403, 'Sorry You are Unauthorized Access To Edit any Domain');
        }
        //dd($request->all());
        $domain = Domain::find($id);
        $request->validate(
            [
                'name' =>  ['required', new DomainName],
                'issuer' => 'required',
                'issue_date' => 'required',
                'expiry_date' => 'required',
                'client_id' => 'required',
            ],
            ['name.required' => 'Please enter a domain name.',    'issuer.required' => 'Please enter domain issuer.',    'issue_date.required' => 'Please enter an issue date.',    'expiry_date.required' => 'Please enter an expiry date.',    'client_id.required' => 'Please choose a client.']

        );

        $domain->name = $request->name;
        $domain->issuer = $request->issuer;
        $domain->issue_date = $request->issue_date;
        $domain->expiry_date = $request->expiry_date;
        $domain->client_id = $request->client_id;
        $domain->status = $request->status;
        $status = $domain->save();
        if ($status) {
            return redirect()->route('domain.index')->with('success', 'domain updated successfully');
        } else {
            return redirect()->back()->with('error', 'problem in updating domain');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (is_null($this->user) || !$this->user->can('domain.edit')) {
            abort(403, 'Sorry You are Unauthorized Access To Delete any Domain');
        }
        $domain = Domain::find($id);
        $status = $domain->delete();
        if ($status) {
            return redirect()->route('domain.index')->with('success', 'domain Deleted successfully');
        } else {
            return redirect()->back()->with('error', 'problem in Deleting domain');
        }
    }
}
