<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Client;
use App\Models\Domain;
use App\Models\Hosting;
use App\Models\HostingCategory;
use Illuminate\Support\Facades\Auth;

class IndexController extends Controller
{
    public $user;
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::guard('admin')->user();
            return $next($request);
        });
    }
    public function index()
    {
        $client = Client::count();
        $domain = Domain::count();
        $expiration_date = now()->addDays(15);
        $expiring_domain = Domain::where('expiry_date', '<=', $expiration_date)->limit(6)->get();
        // dd($expiring_domain);
        $hosting = Hosting::count();

        $expiring_hosting = Hosting::where('expiry_date', '<=', $expiration_date)->with('domain')->with('client')->with('hosting_cat')->limit(6)->get();
        //dd($expiring_hosting);

        $hosting_category = HostingCategory::count();
        if (is_null($this->user) || !$this->user->can('dashboard.view')) {
            abort(403, 'Sorry You are Unauthorized Access To View Dashboard');
        }
        return view('Backend.dashboard.index', compact('client', 'domain', 'hosting', 'hosting_category', 'expiring_hosting','expiring_domain'));
    }
}
