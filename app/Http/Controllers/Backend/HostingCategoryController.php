<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\HostingCategory;
use App\Rules\ValidExpiryDate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HostingCategoryController extends Controller
{
    public $user;
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::guard('admin')->user();
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (is_null($this->user) || !$this->user->can('hostingcategory.view')) {
            abort(403, 'Sorry You are Unauthorized Access To View any Hosting Category');
        }
        $hosting_category = HostingCategory::all();
        return view('Backend.hostingCategory.view', compact('hosting_category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (is_null($this->user) || !$this->user->can('hostingcategory.create')) {
            abort(403, 'Sorry You are Unauthorized Access To Create any Hosting Category');
        }
        return view('Backend.HostingCategory.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (is_null($this->user) || !$this->user->can('hostingcategory.create')) {
            abort(403, 'Sorry You are Unauthorized Access To Create any Hosting Category');
        }
        $request->validate([
            'name' => 'required|max:200',
            'provider' => 'required|max:200',
            'price' => 'required|max:15',
            'issue_date' => 'required',
            'expiry_date' => ['required', new  ValidExpiryDate($request->all())],

        ]);

        $hosting_category = new HostingCategory();
        $hosting_category->name = $request->name;
        $hosting_category->provider = $request->provider;
        $hosting_category->price = $request->price;
        $hosting_category->issue_date = $request->issue_date;
        $hosting_category->expiry_date = $request->expiry_date;
        $status = $hosting_category->save();
        if ($status) {
            return redirect()->route('hostingcategory.index')->with('success', 'Hosting Category added successfully');
        } else {
            return redirect()->back()->with('error', 'problem in adding Hosting Category');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\HostingCategory  $hostingCategory
     * @return \Illuminate\Http\Response
     */
    public function show(HostingCategory $hostingCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\HostingCategory  $hostingCategory
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (is_null($this->user) || !$this->user->can('hostingcategory.edit')) {
            abort(403, 'Sorry You are Unauthorized Access To Edit any Hosting Category');
        }
        $hosting_category = HostingCategory::find($id);
        return view('Backend.HostingCategory.edit', compact('hosting_category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\HostingCategory  $hostingCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (is_null($this->user) || !$this->user->can('hostingcategory.edit')) {
            abort(403, 'Sorry You are Unauthorized Access To Edit any Hosting Category');
        }
        $request->validate([
            'name' => 'required|max:200',
            'provider' => 'required|max:200',
            'price' => 'required|max:15',
            'issue_date' => 'required',
            'expiry_date' => ['required', new  ValidExpiryDate($request->all())],
        ]);

        $hosting_category = HostingCategory::find($id);
        $hosting_category->name = $request->name;
        $hosting_category->provider = $request->provider;
        $hosting_category->price = $request->price;
        $hosting_category->status = $request->status;
        $hosting_category->issue_date = $request->issue_date;
        $hosting_category->expiry_date = $request->expiry_date;
        $status = $hosting_category->save();
        if ($status) {
            return redirect()->route('hostingcategory.index')->with('success', 'Hosting Category updated successfully');
        } else {
            return redirect()->back()->with('error', 'problem in updating Hosting Category');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\HostingCategory  $hostingCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (is_null($this->user) || !$this->user->can('hostingcategory.delete')) {
            abort(403, 'Sorry You are Unauthorized Access To Delete any Hosting Category');
        }
        $hosting_category = HostingCategory::find($id);
        $status = $hosting_category->delete();
        if ($status) {
            return redirect()->route('hostingcategory.index')->with('success', 'Hosting category Deleted successfully');
        } else {
            return redirect()->back()->with('error', 'problem in Deleting Hosting category');
        }
    }
}
