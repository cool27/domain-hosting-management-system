<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Client;
use App\Models\Domain;
use App\Models\Hosting;
use App\Models\HostingCategory;
use App\Models\Payment;
use App\Rules\DomainName;
use App\Rules\IssueDate;
use App\Rules\ValidExpiryDate;
use App\Rules\ValidIssueDate;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Mail\PaymentCompletedMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class HostingController extends Controller
{
    public $user;
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::guard('admin')->user();
            return $next($request);
        });
    }
    public function index()
    {
        if (is_null($this->user) || !$this->user->can('hosting.view')) {
            abort(403, 'Sorry You are Unauthorized Access To View any Hosting Services');
        }
        $hosting = Hosting::with('client')->with('domain')->with('hosting_cat')->with('payment')->get();
        //dd($hosting);
        return view('Backend.hosting.view', compact('hosting'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (is_null($this->user) || !$this->user->can('hosting.create')) {
            abort(403, 'Sorry You are Unauthorized Access To Create any Hosting');
        }
        $client = Client::pluck('id', 'name');
        $hosting_cat = HostingCategory::get();
        //dd($hosting_cat);
        $domain = Domain::pluck('id', 'name');
        return view('Backend.hosting.add', compact('client', 'hosting_cat', 'domain'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if (is_null($this->user) || !$this->user->can('hosting.create')) {
            abort(403, 'Sorry You are Unauthorized Access To Create any Hosting');
        }

        $request->validate([
            'client_id' => 'required',
            'hosting_cat_id' => 'required',
            'external_domain' => [
                'sometimes', // <-- only validate if it exists in the request

            ],
            'issue_date' => ['required'],
            'expiry_date' => ['required', new  ValidExpiryDate($request->all())],
            'selling_price' => 'required'
        ]);

        $hosting = new Hosting();
        $hosting->client_id = $request->client_id;
        $hosting->hosting_cat_id = $request->hosting_cat_id;
        $hosting->domain_id = $request->domain_id;
        $hosting->external_domain = $request->external_domain;
        $hosting->issue_date = $request->issue_date;
        $hosting->expiry_date = $request->expiry_date;
        $hosting->selling_price = $request->selling_price;
        $status = $hosting->save();
        if ($status) {
            return redirect()->route('hosting.index')->with('success', 'hosting added successfully');
        } else {
            return redirect()->back()->with('error', 'problem in adding domain');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (is_null($this->user) || !$this->user->can('hosting.edit')) {
            abort(403, 'Sorry You are Unauthorized Access To edit any Hosting');
        }
        $hosting = Hosting::find($id);
        //dd($hosting);
        $client = Client::pluck('id', 'name');
        $hosting_cat = HostingCategory::get();
        //dd($hosting_cat);
        $domain = Domain::pluck('id', 'name');
        return view('Backend.hosting.edit', compact('client', 'hosting_cat', 'domain', 'hosting'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (is_null($this->user) || !$this->user->can('hosting.edit')) {
            abort(403, 'Sorry You are Unauthorized Access To edit any Hosting');
        }
        $hosting = Hosting::find($id);
        $request->validate([
            'client_id' => 'required',
            'hosting_cat_id' => 'required',
            'external_domain' => [
                'sometimes', // <-- only validate if it exists in the request

            ],
            'issue_date' => ['required'],
            'expiry_date' => ['required'],
            'selling_price' => 'required'
        ]);


        $hosting->client_id = $request->client_id;
        $hosting->hosting_cat_id = $request->hosting_cat_id;
        $hosting->domain_id = $request->domain_id;
        $hosting->external_domain = $request->external_domain;
        $hosting->issue_date = $request->issue_date;
        $hosting->expiry_date = $request->expiry_date;
        $hosting->selling_price = $request->selling_price;
        $hosting->status = $request->status;
        $status = $hosting->save();
        if ($status) {
            return redirect()->route('hosting.index')->with('success', 'hosting updated successfully');
        } else {
            return redirect()->back()->with('error', 'problem in updated hosting');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (is_null($this->user) || !$this->user->can('hosting.edit')) {
            return response()->json(['message' => 'Unauthorized'], 403);
        }
        $hosting = Hosting::find($id);
        if (!$hosting) {
            return response()->json(['message' => 'Hosting not found'], 404);
        }
        $status = $hosting->delete();
        if ($status) {
            return response()->json(['message' => 'Hosting Deleted successfully'], 200);
        } else {
            return response()->json(['message' => 'Problem in deleting Hosting'], 500);
        }
    }
    public function ViewInvoice($id)
    {
        $hosting = Hosting::find($id)->with('client')->with('domain')->with('hosting_cat')->first();

        return view('Backend.hosting.invoice', compact('hosting'));
    }
    public function MakePayment(Request $request)
    {


        $hosting = Hosting::where('id', $request->hosting_id)->first();
        $ClientEmail = $hosting->client->email;
        //dd($ClientEmail);
        $validator = Validator::make($request->all(), [
            'payment_amount' => 'required|numeric|max:' . $hosting->selling_price,
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $dueAmount = $hosting->selling_price - $request->payment_amount;
        if ($dueAmount) {
            $paymenttype = 'partial';
        } else {
            $paymenttype = 'full';
        }
        $saveHosting = new Payment();
        $saveHosting->hosting_id = $request->hosting_id;
        $saveHosting->payment_status = $paymenttype;
        $saveHosting->payment_amount = $request->payment_amount;
        $saveHosting->due_amount = $dueAmount;
        Mail::to($ClientEmail)->send(new PaymentCompletedMail($saveHosting));
        $status = $saveHosting->save();
        if ($status) {
            return redirect()->route('hosting.index')->with('success', 'payment made successfully');
        } else {
            return redirect()->back()->with('error', 'problem in making payment');
        }

        //dd($paymenttype);
        //dd($dueAmount);
        //dd($hosting);
    }
    public function UpdatePayment(Request $request, $id)
    {



        //dd($NewpaymentAmount);
        $hosting = Hosting::where('id', $request->hosting_id)->first();
        $Findhosting = Payment::find($id);
        $PreviousDue = $Findhosting->due_amount;

        $NewpaymentAmount = $Findhosting->payment_amount + $request->payment_amount;
        //dd($NewpaymentAmount);
        $dueAmount = $hosting->selling_price - $NewpaymentAmount;
        //dd($dueAmount);
        //dd($dueAmount);
        $validator = Validator::make($request->all(), [
            'payment_amount' => 'required|numeric|max:' . $PreviousDue,
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }
        if ($dueAmount) {
            $paymenttype = 'partial';
        } else {
            $paymenttype = 'full';
        }

        $Findhosting->payment_status = $paymenttype;
        $Findhosting->payment_amount = $NewpaymentAmount;
        $Findhosting->due_amount = $dueAmount;
        $status = $Findhosting->save();
        if ($status) {
            return redirect()->route('hosting.index')->with('success', 'payment made successfully');
        } else {
            return redirect()->back()->with('error', 'problem in making payment');
        }
    }
}
