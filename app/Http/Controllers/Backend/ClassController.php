<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\ClassesRequest;
use App\Models\Classes;
use App\Repositories\Classes\ClassRepository;
use App\Repositories\Sections\SectionRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ClassController extends Controller
{
    public $classRepository;
    public $sectionRepository;
    public $user;
    public function __construct(ClassRepository $classRepository, SectionRepository $sectionRepository)
    {
        $this->classRepository = $classRepository;
        $this->sectionRepository = $sectionRepository;

        $this->middleware(function($request,$next){
            $this->user=Auth::guard('admin')->user();
            return $next($request);
        });

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(is_null($this->user)||!$this->user->can('class.view') ){
        abort(403,'Sorry You are Unauthorized Access To View any Class');
    }
        $classes = $this->classRepository->getAllClass();
        return view('Backend.class.view', compact('classes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(is_null($this->user)||!$this->user->can('class.create') ){
            abort(403,'Sorry You are Unauthorized Access To Create any Class');
        }
        $sections = $this->sectionRepository->getAllSection();
        //dd($section);
        return view('Backend.class.add', compact('sections'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClassesRequest $request)
    {
        if(is_null($this->user)||!$this->user->can('class.create') ){
            abort(403,'Sorry You are Unauthorized Access To Create any Class');
        }
        $class = $this->classRepository->addClass($request->all());
        if ($class) {
            return redirect()->route('class.index')->with('success', 'Class Added Successfully');
        } else {
            return redirect()->route('class.create')->with('error', 'Error in Adding Class');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $classId
     * @return \Illuminate\Http\Response
     */
    public function edit($classId)
    {
        if(is_null($this->user)||!$this->user->can('class.edit') ){
            abort(403,'Sorry You are Unauthorized Access To Edit any Class');
        }
        $sections = $this->sectionRepository->getAllSection();
        $class = $this->classRepository->getSingleClass($classId);
        return view('Backend.class.edit', compact('class', 'sections'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $classId
     * @return \Illuminate\Http\Response
     */
    public function update(ClassesRequest $request, $classId)
    {

        if(is_null($this->user)||!$this->user->can('class.edit') ){
            abort(403,'Sorry You are Unauthorized Access To Edit any Class');
        }
        $class = $this->classRepository->updateClass($request->all(), $classId);
        if ($class) {
            return redirect()->route('class.index')->with('success', 'Class Updated Successfully');;
        } else {
            return redirect()->route('class.index')->with('error', 'Error in Updating Class');;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($classId)
    {

        if(is_null($this->user)||!$this->user->can('class.edit') ){
            abort(403,'Sorry You are Unauthorized Access To Delete any Class');
        }
        $class = $this->classRepository->deleteClass($classId);
        if ($class) {
            return redirect()->route('class.index')->with('success', 'Class Deleted Successfully');
        } else {
            return redirect()->route('class.index')->with('error', 'Error in Deleting Class');
        }
    }
}
