<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;

class ClientController extends Controller
{
    public $user;
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::guard('admin')->user();
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (is_null($this->user) || !$this->user->can('client.view')) {
            abort(403, 'Sorry You are Unauthorized Access To View any CLient');
        }
        $client = Client::all();
        //dd($client);
        return view('Backend.client.view', compact('client'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (is_null($this->user) || !$this->user->can('client.create')) {
            abort(403, 'Sorry You are Unauthorized Access To View any Client');
        }
        return view('Backend.client.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (is_null($this->user) || !$this->user->can('client.create')) {
            abort(403, 'Sorry You are Unauthorized Access To View any Client');
        }
        $request->validate([
            'inputs.*.name' => 'required|max:200',
            'inputs.*.email' => 'required|email',
            'inputs.*.phone' => 'required',
            'inputs.*.address' => 'max:200',
        ],
        [
            'inputs.*.name.required'=>'client name is required',
            'inputs.*.email.required'=>'client email is required',
            'inputs.*.phone.required'=>'client phone is required',
        ]
    );

        foreach($request->inputs as $key=>$value)
        {
        $status=Client::create($value);

        }
        if ($status) {
            return redirect()->route('client.index')->with('success', 'client added successfully');
        } else {
            return redirect()->back()->with('error', 'problem in adding client');
        }

        // $clients = new Client();
        // $clients->name = $request->name;
        // $clients->email = $request->email;
        // $clients->phone = $request->phone;
        // $clients->address = $request->address;
        // $status = $clients->save();
        // if ($status) {
        //     return redirect()->route('client.index')->with('success', 'client added successfully');
        // } else {
        //     return redirect()->back()->with('error', 'problem in adding client');
        // }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (is_null($this->user) || !$this->user->can('client.edit')) {
            abort(403, 'Sorry You are Unauthorized Access To Edit any Client');
        }
        $client = Client::find($id);
        //dd($client);
        return view('Backend.client.edit', compact('client'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (is_null($this->user) || !$this->user->can('client.edit')) {
            abort(403, 'Sorry You are Unauthorized Access To Edit any Client');
        }
        $request->validate([
            'name' => 'required|max:200',
            'email' => 'required|email',
            'phone' => 'required',
            'address' => 'max:200',
        ]);

        $clients = Client::find($id);
        $clients->name = $request->name;
        $clients->email = $request->email;
        $clients->phone = $request->phone;
        $clients->address = $request->address;
        $clients->status = $request->status;
        $status = $clients->save();
        if ($status) {
            return redirect()->route('client.index')->with('success', 'client updated successfully');
        } else {
            return redirect()->back()->with('error', 'problem in updating client');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (is_null($this->user) || !$this->user->can('client.delete')) {
            abort(403, 'Sorry You are Unauthorized Access To Delete any Client');
        }
        $client = Client::find($id);
        $status = $client->delete();
        if ($status) {
            return redirect()->route('client.index')->with('success', 'client Deleted successfully');
        } else {
            return redirect()->back()->with('error', 'problem in Deleting client');
        }
    }
}
